**This is the home page of [this documentation](http://wonderland.eode.studio/docs/colormatrix-_introduction.html)**

## Introduction

### Version
> Released, actively maintained

### Dependencies
- `Common`

## What is it ?
A color matrix defines a set of simple modifications for each pixel of an image. Effect informations are grouped into 16 values. We "multiply" the value of a pixel by the matrix to apply the changes.
The changes can be:
- Hue rotation
- Saturation
- Lighness
- Inversion
- RGB Swap
- Custom effects (sepia, black and white...)
- Target a color range

![Example](http://wonderland.eode.studio/ColorMatrix/intro.png)
*Mario assets : http://pixeljoint.com/pixelart/79091.htm*

## Why ColorMatrix instead of a standard shader system ?
The matrices have the main advantage of being mergeable which makes it possible to combine several effects easily or to inherit them between the objects of the scene. Calculations done are also more stable because whatever the number of effects are applied there will always be the same number of calculations made for the rendering.

## When to use it ?
- In 2D games, you can change skins, ambience, effects, icons and create themes (normal -> boss -> night...), allow player to change its items colors...
- In 3D games, it's less useful. Because you use specific shaders, lights and effects to create a realistic environment. It can be used to change the texture color for a skin system (painted walls, dyed garment...).


## Thanks
- For basic algorithms `<3` : http://www.graficaobscura.com/matrix/
- For matrices examples: https://docs.rainmeter.net/tips/colormatrix-guide/